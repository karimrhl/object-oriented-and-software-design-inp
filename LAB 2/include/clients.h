#ifndef clients_H
#define clients_H
#include<iostream>
#include<string.h>
#include "account.h"
using namespace std;
class clients{
 private:
  string name;
  unsigned int id;
  current_accounts* p_ca;
  unblocked_accounts* p_ua;
 public:
  clients(string n,unsigned int i,unsigned int bank_id,
  float current_balance,unsigned int a10,float sum1){
  p_ca=new current_accounts(bank_id,current_balance);
  p_ua=new unblocked_accounts(a10,sum1);
    name=n;
    id=i;
  }
  clients(const clients &A);
  unsigned int get_id();
  void creditca(float a);
void creditsa(float a);
void debitsa(float a);

void debitca(float a);

void print(ostream &o);
};
#endif //clients_H


