#ifndef account_H
#define account_H
#define IRATBLOCKED 4
#define IRATUNBLOCKED 2
#include<iostream>
#include<string>
using namespace std;

class banking_account{

protected:

   unsigned int bank_id;
   float current_balance;

public:

 banking_account(unsigned int a=0,float sum=0){
        bank_id=a;
current_balance=sum;}

void credit(float sum);

double debit(double sum);
float get_current_balance();
void set_current_balance(float a);

unsigned int g1();
/*void print(){
cout<<"id: "<<bank_id<<endl;
cout<<"balance: "<< current_balance;}
};*/
void print(ostream& o);
};

class current_accounts: public banking_account{

public:
current_accounts(unsigned int a=0,float sum=0)
:banking_account(a,sum)
{
}

double debit(float sum);
};

class savings_accounts: public banking_account{
 private:
  float interest_rate;
 public:
    savings_accounts(unsigned int a=0,float sum=0,float i=0)
    :banking_account(a,sum)
    { interest_rate=i;
    }
    void add_interest();
    };
class blocked_accounts: public savings_accounts{
public:
   blocked_accounts(unsigned int a=0,float sum=0):savings_accounts(a,sum,0.04){}
   double debit(float sum);
      };
class unblocked_accounts: public savings_accounts{
public:
   unblocked_accounts(unsigned int a=0,float sum=0):savings_accounts(a,sum,0.02){}

     double debit(float sum);
};
#endif //account_H
