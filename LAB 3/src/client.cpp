#include "client.hpp"


Client::Client(std::string n, unsigned int i, current_account *c, unblocked_account *s){
  name = n;
  id = i;
  current = c;
  savings = s;
}
unsigned int Client::get_id(){
  return id;
}
void Client::credit_ca(double a){
  current->credit(a);
}
void Client::credit_sa(double a){
  savings->credit(a);
}
double Client::debit_ca(double a){
  current->debit(a);
}
double Client::debit_sa(double a){
  savings->debit(a);
}
