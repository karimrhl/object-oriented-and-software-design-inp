#include <string>
#include "account.hpp"

class Client{
  friend std::ostream &operator<<(std::ostream &, const Client &);
  private:
    std::string name;
    unsigned int id;
    current_account *current;
    unblocked_account *savings;
  public:
    /* Constructor*/
    Client(std::string n, unsigned int i, current_account *c, unblocked_account *s);
    /* Methods */
    unsigned int get_id();
    void credit_ca(double a);
    void credit_sa(double a);
    double debit_ca(double a);
    double debit_sa(double a);
};
