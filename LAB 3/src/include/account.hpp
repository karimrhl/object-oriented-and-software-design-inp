#include <iostream>

#define IRATEBLOCKED 4
#define IRATEUNBLOCKED 2

class banking_account {
  friend std::ostream &operator<<(std::ostream &o, const banking_account &b);
  private:
    int bank_id;
  protected:
    double balance;
  public:
    /* Constructor*/
    banking_account(int id, double b);
    /* Methods */
    void credit(double a);
    double debit(double a);
    virtual void print(std::ostream &o) const;

};

class current_account : public banking_account {
  public:
    /* Constructor*/
    current_account(int bank_id, double balance);
    /* Methods */
    virtual double debit(double a);
    virtual void print(std::ostream &o) const;
};

class saving_account : public banking_account {
  private:
    float interest_rate;
  public:
    /* Constructor*/
    saving_account(int bank_id, double balance, float interest);
    /* Methods */
    void add_interests();
    virtual void print(std::ostream &o) const;
};

class blocked_account : public saving_account {
  public:
    /* Constructor*/
    blocked_account(int bank_id, double balance, float interest);
    /* Methods */
    double debit(double a);
    virtual void print(std::ostream &o) const;
};

class unblocked_account : public saving_account {
  public:
    /* Constructor*/
    unblocked_account(int bank_id, double balance, float interst);
    /* Methods */
    double debit(double a);
    virtual void print(std::ostream &o) const;
};
