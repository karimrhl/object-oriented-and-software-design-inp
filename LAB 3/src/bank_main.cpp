#include "bank.hpp"

std::ostream &operator<<(std::ostream &o, const banking_account &b){
  b.print(o);
  return o;
}

std::ostream &operator<<(std::ostream &os, const Client &client){
  os<<"Name : "<<client.name<<" - Identifier : "<<client.id<<std::endl;
  if (client.current != NULL) {
    os<<*(client.current);
  }
  if (client.savings != NULL) {
    os<<*(client.savings);
  }
  return os;
}

std::ostream &operator<<(std::ostream &os, const Bank &B){
  if (B.clients.size() != 0) {
    for (std::map<unsigned int, Client*>::const_iterator i = B.clients.begin();
      i!= B.clients.end(); i++) {
        os<<*(i->second)<<std::endl;
    }
  }
  else {
    os<<"The bank has no client"<<std::endl;
  }
  return os;
}

int main(){
  unblocked_account M_savings(110, 10000.2, IRATEUNBLOCKED);
  current_account M_moneymoney(265, 40.9);
  unblocked_account D_savings(496, 526.3, IRATEUNBLOCKED);
  current_account D_moneymoney(526, 10.5);

  Client Marjolaine("Marjolaine", 32, &M_moneymoney, &M_savings);
  Client Daria("Daria", 48, &D_moneymoney, &D_savings);

  Bank B;
  std::cout<<"We initialize the bank : \n";
  std::cout<<B;

  std::cout<<"\nWe now insert two clients in the bank :\n";

  B.insertClient(&Marjolaine);
  B.insertClient(&Daria);
  std::cout<<B;
}
