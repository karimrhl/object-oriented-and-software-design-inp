#include <iostream>
#include <cmath>
#include <vector>
using namespace std;

class Poly: public Function {
	private:
		vector<double> coef;
	public:
		Poly(int order, double* k): Function("Polynomial") {
			for (int i=0;i<order;i++) coef.push_back(k[i]);

		}
		double eval(double x0) {
			double total = 0;
			for (int i=0;i<coef.size();i++)
				total += coef[i] * pow(x0, i);
			return total;
		}
		void print(ostream &o) {
			Function::print(o);
			o<<"f(x) = ";
			for (int i=0;i<coef.size();i++)
				o<<coef[i]<<"*x^"<<i<<" + ";
			o<<endl;
		}
		Poly* derivative() {
			double *k1 = new double[coef.size()-1];
			for (int i=1;i<coef.size();i++)
				k1[i-1] = coef[i]*i;
			return new Poly(coef.size()-1, k1);
		}
};

