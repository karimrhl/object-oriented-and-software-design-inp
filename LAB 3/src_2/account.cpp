#include "account.hpp"

banking_account::banking_account(int id, double b){
  bank_id = id;
  balance = b;
}

void banking_account::credit(double a){
  balance += a;
}

double banking_account::debit(double a){
  balance -= a;
  return a;
}

void banking_account::print(std::ostream &o) const {
  o<<"Account n° "<<bank_id<<" - Balance : "<<balance<<" €."<<std::endl;
}

current_account::current_account(int id, double b) : banking_account(id, b){}

double current_account::debit(double a){
  if (balance > a){
    balance -= a;
    return a;
  }
  else if (balance > 0){
    int b = balance;
    balance = 0;
    return b;
  }
  else if (balance == 0){
    std::cout<<"No debit is possible"<<std::endl;
    return 0;
  }
}

void current_account::print(std::ostream &o) const {
  o<<"** Current account **"<<std::endl;
  banking_account::print(o);
}

saving_account::saving_account(int id, double b, float interest) : banking_account(id, b){
  interest_rate = interest;
}

void saving_account::add_interests(){
  balance += balance*interest_rate/100;
}

void saving_account::print(std::ostream &o) const {
  o<<"** Saving account **"<<std::endl;
  banking_account::print(o);
}

blocked_account::blocked_account(int id, double b, float interest) :
saving_account(id, b, interest){}

double blocked_account::debit(double a){
  std::cout<<"Account blocked, no debit possible"<<std::endl;
  return -1;
}

void blocked_account::print(std::ostream &o) const {
  o<<"** Blocked saving account **"<<std::endl;
  banking_account::print(o);
}

unblocked_account::unblocked_account(int id, double b, float interest) :
saving_account(id, b, interest){}

double unblocked_account::debit(double a){
    if (balance > a){
      balance -= a;
      return a;
    }
    else if (balance == 0){
      std::cout<<"Not enough money to perform the debit"<<std::endl;
      return 0;
    }
}

void unblocked_account::print(std::ostream &o) const {
  o<<"** Unblocked saving account **"<<std::endl;
  banking_account::print(o);
}
