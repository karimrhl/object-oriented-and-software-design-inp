#include "bank.hpp"
#include <fstream>

std::ostream &operator<<(std::ostream &o, const banking_account &b){
  b.print(o);
  return o;
}

std::ostream &operator<<(std::ostream &os, const Client &client){
  os<<"Name : "<<client.name<<" - Identifier : "<<client.id<<std::endl;
  for (std::vector<banking_account *>::const_iterator i = client.accounts.begin(); i != client.accounts.end(); i++) {
      os<<**i<<std::endl;
  }
  return os;
}

std::ostream &operator<<(std::ostream &os, const Bank &B){
  if (B.clients.size() != 0) {
    for (std::map<unsigned int, Client*>::const_iterator i = B.clients.begin();
      i!= B.clients.end(); i++) {
        os<<*(i->second)<<std::endl;
    }
  }
  else {
    os<<"The bank has no client"<<std::endl;
  }
  return os;
}

int main(){
  Client Marjolaine("Marjolaine", 32, 2);
  Client Daria("Daria", 48, 1);

  Bank B;
  std::cout<<"-----------We initialize the bank-----------\n";
  std::cout<<B;

  std::cout<<"\n-----------We now insert two clients in the bank-----------\n";

  B.insertClient(&Marjolaine);
  B.insertClient(&Daria);
  std::cout<<B;

  std::cout<<"\n-----------We create accounts for Marjolaine and Daria-----------\n";
  Marjolaine.createAccount();
  Marjolaine.createAccount();
  Daria.createAccount();
  std::cout<<"\n-----------The bank now looks like this-----------\n";
  std::cout<<B;

  std::cout<<"\n-----------We try to create a second account for Daria-----------\n";
  Daria.createAccount();

  /* Let us display the content of the bank in a file */
  std::ofstream myfile;
  myfile.open ("bank.txt");
  myfile << B;
  myfile.close();

  printf("You can now find the content of the bank in the file bank.txt\n");

  return 0;

}
