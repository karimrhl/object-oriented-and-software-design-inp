#include "client.hpp"
#include <cstring>

Client::Client(std::string n, unsigned int i, unsigned int m){
  name = n;
  id = i;
  max_acc = m;
  nb_acc = 0;
}

Client::Client(const Client &C) {
    name = C.name ;
    id = C.id ;
    max_account = C.max_account ;
    current_number = C.current_number ;

    if (current_number > 0) {
        //checking what type of account we have to create and copy the right type for each element in the vector
        for (unsigned int i = 0 ; i < current_number ; i++) {
            if (typeid(*C.accounts[i]) == typeid(Current_account)) {
                Current_account C_new1 = new Current_account(C.accounts[i]->get_bank_id(), C.accounts[i]->get_balance()) ;
                accounts.push_back(C_new1) ;
            }
            else if (typeid(C.accounts[i]) == typeid(Unblocked_sav_account)) {
                Unblocked_sav_account C_new2 = new Unblocked_sav_account(C.accounts[i]->get_bank_id(), C.accounts[i]->get_balance()) ;
                accounts.push_back(C_new2) ;
            }
            else if (typeid(C.accounts[i]) == typeid(Blocked_sav_account)) {
                Blocked_sav_account *C_new3 = new Blocked_sav_account(C.accounts[i]->get_bank_id(), C.accounts[i]->get_balance()) ;
                accounts.push_back(C_new3) ;
            }
        }
    }
}
// Client::Client(const Client& c){
//   name = new char[strlen(c.name)+1];
//   strcpy(name,c.name);
//   id = c.id;
//   accounts = c.accounts;
//   max_acc = c.max_acc;
//   nb_acc = c.nb_acc;
// }
unsigned int Client::get_id(){
  return id;
}
void Client::createAccount(){
  if (nb_acc == max_acc) {
    std::cout<<"You cannot create another account for "<<name<<"."<<std::endl;
  }
  else
  std::cout<<"Creation of accounts for "<<name<<" :\nWhat type of account do you want to create ?"<<
    "\nCurrent account : 1"<<
    "\nBlocked saving account : 2"<<
    "\nUnblocked saving account : 3"<<std::endl;
  int type;
  std::cin>>type;
  if (type == 1) {
    current_account *acc = new current_account(nb_acc, 0);
    accounts.push_back(acc);
    nb_acc++;
  }
  else if (type == 2) {
    blocked_account *acc = new blocked_account(nb_acc, 0, IRATEBLOCKED);
    accounts.push_back(acc);
    nb_acc++;
  }
  else if (type == 3) {
    unblocked_account *acc = new unblocked_account(nb_acc, 0, IRATEUNBLOCKED);
    accounts.push_back(acc);
    nb_acc++;
  }
  else {
    std::cout<<"Invalid entry"<<std::endl;
  }
}
void Client::credit(double a, int k){
  accounts[k]->credit(a);
}
double Client::debit(double a, int k){
  accounts[k]->debit(a);
}
