#include <map>
#include "client.hpp"

class Bank{
    friend std::ostream &operator<<(std::ostream &, const Bank &);
  private:
    std::map<unsigned int, Client*> clients;
  public:
    void insertClient(Client*);
};
