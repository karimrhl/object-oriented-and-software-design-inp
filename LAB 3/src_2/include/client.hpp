#include <string>
#include <vector>
#include "account.hpp"

class Client{
  friend std::ostream &operator<<(std::ostream &, const Client &);
  private:
    std::string name;
    unsigned int id;
    std::vector<banking_account *> accounts;
    unsigned int max_acc, nb_acc;
  public:
    /* Constructor*/
    Client(std::string n, unsigned int i, unsigned int m);
    Client(const Client& c);
    /* Methods */
    unsigned int get_id();
    void createAccount();
    void credit(double a, int k);
    double debit(double a, int k);
};
