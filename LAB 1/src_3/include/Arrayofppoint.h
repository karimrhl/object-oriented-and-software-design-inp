
#include "ppoint.h"
#include <ctime>

    class Array_of_PPoint {

    private:
        int len;
        PPoint *A;
public:


    Array_of_PPoint(int len1) {
        A= new PPoint[len1];
        len = len1;
        for (int i = 0; i < len1; i++) {
                A[i] = PPoint(0,0);
        }
        cout<<"calling copy constructor"<<endl;
    }
    ~Array_of_PPoint(){
    delete A;
    cout<<" Calling destructor"<<endl;
    }

    float RandomFloat(float a, float b) {
        float random = ((float) rand()) / (float) RAND_MAX;
        float diff = b - a;
        float r = random * diff;
        return a + r;
    }

    Array_of_PPoint(int len, float value_square1, float value_square2){
        PPoint A1[len] ;
        for (int i = 0; i < len; i++){
            float x = RandomFloat(value_square1, value_square2);
            float y = RandomFloat(value_square1, value_square2);
            A1[i].set_coordinates(x,y);
        }
    }

    void add(const PPoint& point);


    void print_tab();




};
