#ifndef PPOINT_H
#define PPOINT_H
#include<iostream>
#include<typeinfo>
using namespace std;
class PPoint {
     private: int *x;
             int *y;
     public:
        PPoint(int x=0, int y=0);
        PPoint(const PPoint& P);
        ~PPoint();
        void add(const PPoint &P);
        void print_point();
        void set_coordinates(int x=0, int y=0);
};

#endif
