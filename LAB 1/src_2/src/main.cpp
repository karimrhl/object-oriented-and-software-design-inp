#include <iostream>
#include "Date.h"
#include "Trip.h"
using namespace std;

int main() {
    Date today = Date(5,9,2021);
    Date end_trip = Date(30,9,2021);
    Date *ptr_today = &today;
    Date *ptr_end_trip = &end_trip;

       time_t timestamp = time( NULL );
       struct tm * now = localtime( & timestamp );
       today.PrintDate();
       today.HappyBirthday("Karim",1,4,12);

    Trip first_trip = Trip(21,9,2021,30,9,2021,1000);
    first_trip.print_Trip();

    Trip second_trip = Trip(ptr_today,ptr_end_trip,100000);
    second_trip.print_Trip();

    cout << "The price per day is : "<<second_trip.price_per_day()<<endl;


    cout << "We expect 0 : " << before(end_trip, today) << endl << endl;
    cout << "We expect 1 : " << before(today, end_trip) << endl << endl;

    cout << "We expect 25 : " << difference(end_trip, today) << endl << endl;

    cout << "We expect 25 : " << duration(end_trip, today) << endl << endl;
    cout << "We expect 25 : " << duration(today, end_trip) << endl << endl;

    return 0;
}
