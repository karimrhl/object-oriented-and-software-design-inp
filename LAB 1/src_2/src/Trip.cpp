
#include "Trip.h"
#include <assert.h>


void Trip::print_Trip(){
    std::cout <<"The trip begins the ";
    beginningDate->PrintDate();
    std::cout <<"The trip ends the ";
    endDate->PrintDate();
    std::cout <<"The price is "<<price << "$." <<std::endl;

}

float Trip::price_per_day() {

    if (before(*beginningDate, *endDate)) {

        int duration1 = duration(*endDate, *beginningDate);

        return price / duration1;
    }
    else
        return 0;
}


Date Trip::get_beg_date(){return *(this->beginningDate);}

Date Trip::get_end_date(){return *(this->endDate);}

float Trip::get_price(){return this->price;}

//bool before(Date d1, Date d2){
bool before(Date &d1, Date &d2){
    if (d1.get_year()>d2.get_year())
        return false;
    if (d1.get_month()>d2.get_month())
        return false;
    if (d1.get_day()>=d2.get_day())
        return false;

    return true;
}

//int difference(Date d1, Date d2){
int difference(Date &d1, Date &d2){
    int val = d1.get_day() - d2.get_day();
    val += 30*(d1.get_month() - d2.get_month());
    val += 12*30*(d1.get_year() - d2.get_year());
    return val;
}

//int duration(Date d1, Date d2){
int duration(Date &d1, Date &d2){
    if (before(d1, d2))
        return difference(d2, d1);
    else
        return difference(d1, d2);
}

