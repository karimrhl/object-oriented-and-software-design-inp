#ifndef UNTITLED12_TRIP_H
#define UNTITLED12_TRIP_H

#include "Date.h"

class Trip {
    private:
        Date *beginningDate;
        Date *endDate;
        float price;
    public:

        Trip(int day_begin,int month_begin,int year_begin,int day_end,int month_end,int year_end, float price1){
            beginningDate = new Date(day_begin,month_begin,year_begin);
            endDate = new Date(day_end,month_end,year_end);
            price = price1;
        }

        Trip(Date *beginningDate1, Date *endDate1, float price1)
        {
            beginningDate = beginningDate1;
            endDate = endDate1;
            price= price1;
        }

        Date get_beg_date();
        Date get_end_date();
        float get_price();

        void print_Trip();

        float price_per_day();

};

//pass-by-value
/*bool before(Date d1, Date d2);
int difference(Date d1, Date d2);
int duration(Date d1, Date d2);
*/
//pass-by-reference
bool before(Date &d1, Date &d2);
int difference(Date &d1, Date &d2);
int duration(Date &d1, Date &d2);


#endif //UNTITLED12_TRIP_H
