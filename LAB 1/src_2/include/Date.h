
#include <iostream>
#include <cstring>
using namespace std;

#ifndef UNTITLED12_DATE_H
#define UNTITLED12_DATE_H

#include <time.h>

class Date {
private :
    int day;
    int month;
    int year;
    char* dayofweek;

public :
    Date(int day1, int month1, int year1){
        day = day1;
        month = month1;
        year = year1;
        dayofweek = new char;
        cout << "Call of the pass-by-value constructor." << endl;}

    Date (tm *date){
        day = date->tm_mday;
        month = date->tm_mon;
        year = date->tm_year;
        dayofweek = new char;
        cout << "Call of the pass-by-value constructor (with library tm)." << endl;}


    Date(const Date& d){
        day = d.day;
        month = d.month;
        year = d.year;
        dayofweek = new char;
        strcpy(dayofweek, d.dayofweek);
        cout << "Call of the copy constructor." << endl;}

    ~Date(){
        delete[] dayofweek;}


    int get_day();
    int get_month();
    int get_year();
    char* get_dayofweek();

    void PrintDate();

    void HappyBirthday(char * name, int day1, int month1, int year1);


};


#endif //UNTITLED12_DATE_H
