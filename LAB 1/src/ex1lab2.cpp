#include <iostream>
using namespace std;
//pass by value
int make_computation_by_value(int x) {
 int i;
 for (i=1; i<=4; i++)
 x = x * i;
 return x;
}
 int make_computation_by_refernce(int &x){
 int i;
 for (i=1; i<=4; i++)
 x = x * i;
 return x;
 }
 int make_computation_by_pointer(int *x) {
 int i;
 for (i=1; i<=4; i++)
 *x = *x * i;
 return *x;

}

int main(void) {
 int k;
 printf("Give an integer for the computation ");
 cin >> k;
 cout << "Function make_computation_by_value returns " << make_computation_by_value(k)
 << " and the value of k is " << k << endl;
 cout << "Function make_computation_by_refernce returns " << make_computation_by_refernce(k)
 << " and the value of k is " << k << endl;
 cout << "Function make_computation_by_pointer returns " << make_computation_by_pointer(&k)
 << " and the value of k is " << k << endl;
 return 0;
}

